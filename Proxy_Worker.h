#ifndef _PROXYWORKER_H_
#define _PROXYWORKER_H_

#include <iostream>
#include <string>

#include <queue>

#include "HTTP_Response.h"
#include "HTTP_Request.h"
#include "TCP_Socket.h"
#include "URI.h"


/*
 * Purpose: Acts as a single worker to handle a request from a proxy. It 
 *          does not cache anything in this lab. It just pass the requst
 *          to the server and return the response to the client.
*/
class Proxy_Worker {
private:
    URI                *server_uri;      // Server's URI, obtained from
                                         // each request.
    unsigned short int port;             // For a full blown proxy

    // ADD SOME MEMBER VARIALBE
    // 1. resize and quality
    std::string resize;
    std::string quality;
    // 2. the slave list
    std::queue<TCP_Socket*> slaves;

    HTTP_Request *masterRequest;

    TCP_Socket         *client_sock;     // The socket for client
    TCP_Socket         server_sock;
    HTTP_Request       *client_request;  // Obj to handle client request
    HTTP_Response      *server_response; // Obj to handle server response

    // Details for these functions in Proxy_Worker.cc
    bool get_request_from_client();
    bool forward_request_to_server();
    bool return_response_to_client();
    bool return_response_to_client(int);
    int get_chunk_size(std::string &data);

    void broadcast_response_to_slaves(HTTP_Response *);

public:
    Proxy_Worker(TCP_Socket *master_sock, std::string resize, std::string quality, HTTP_Request* request, std::queue<TCP_Socket*> slaveSocks);
    ~Proxy_Worker();

    void handle_request();
};

#endif
