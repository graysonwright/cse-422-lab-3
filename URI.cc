#include "URI.h"
#include <cstdlib>
#include <sstream>

using namespace std;


namespace
{
    // Used to signal when the port number is not known.  There is an
    // excellent chance that this will never be a valid port for anything.
    const unsigned UNDEFINED_PORT = 0xffffffff;
}

/*********************************
 * Name:    URI
 * Purpose: constructor of URI class objects
 * Reecive: none
 * Return:  none
 *********************************/
// We want to assume that the port hasn't been set until we know otherwise.
// We also want to make sure that there's some kind of path, since HTTP
// requires a path.  The root path is the accepted default there.
URI::URI()
    : port(UNDEFINED_PORT),
    path("/")
{
}

/*********************************
 * Name:    ~URI
 * Purpose: destructor of URI class objects
 * Receive: none
 * Return:  none
 *********************************/
URI::~URI()
{
    // Nothing to do...
}

/*********************************
 * Name:    parse
 * Purpose: parse the uri string and create an URI object
 * Receive: uri_string: the URI string to be parsed
 * Return:  pointer to the URI object
 *********************************/
URI* URI::parse(const string& uri_string)
{

    URI* new_uri = new URI();

    // Obtain the protocol from uri_string
    size_t offset = new_uri->read_protocol(uri_string);
    if (offset == string::npos)
    {
        delete new_uri;
        return NULL;
    }

    // Obtain the port from uri_string
    offset = new_uri->read_host_port(uri_string, offset);

    // if the offset has not yet read the end of the uri string,
    // get the path
    if (offset < uri_string.length())
    {
        // Obtain the path
        new_uri->read_path_details(uri_string, offset);

        // If the client somehow input a URI with an empty path,
        // quietly save them from themselves.
        if (new_uri->path.length() == 0)
        {
            new_uri->path = "/";
        }
    }

    return new_uri;
}

const string& URI::get_protocol() const
{
    return protocol;
}

const string& URI::get_host() const
{
    return host;
}

bool URI::is_port_defined() const
{
    return (port != UNDEFINED_PORT);
}

unsigned URI::get_port() const
{
    return port;
}

const string& URI::get_path() const
{
    return path;
}

const string& URI::get_query() const
{
    return query;
}

const string& URI::get_fragment() const
{
    return fragment;
}

void URI::Print(ostream& out)
{
    // Say the URI is http://www.example.org:8080/example.php?example#ex
    // Each piece follows.  Note that we should avoid printing optional
    // parts of the URI that have associated formatting characters, if
    // they aren't actually defined.

    // http://
    out << protocol << "://";

    // www.example.org
    out << host;

    // :8080 (if given)
    if (is_port_defined())
        out << ":" << port;

    // /example.php
    out << path;

    // ?example(if given)
    if (query.length() > 0)
        out << "?" << query;

    // #ex (if given)
    if (fragment.length() > 0)
        out << "#" << fragment;
}

void URI::Print(string& target)
{
    // Much easier than duplicating the code.
    ostringstream target_out;
    Print(target_out);
    target = target_out.str();
}

/*********************************
 * Name:    read_protocol
 * Purpose: read the protocol from the URI string
 * Receive: uri_string: the URI string to be parsed 
 *          offset:     the offset, the position to start parsing
 *                      default = 0
 * Return:  the offset indicates that the part before this offset
 *          has been parsed
 *********************************/
size_t URI::read_protocol(const string& uri_string, size_t offset)
{
    size_t protocol_end = uri_string.find("://", offset);

    if (protocol_end == string::npos) // If protocol is not specified
    {                                 // assume it is http
        protocol = "http";
        return 0;
    }
    else{
        protocol = uri_string.substr(offset, protocol_end - offset);
        return protocol_end + 3;
    }
}

/*********************************
 * Name:    read_host_port
 * Purpose: read the port from the URI string, if specified
 * Receive: uri_string: the URI string to be parsed
 *          offset:     the offset, the position to start parsing
 * Return:  the offset indicates that the part before this offset
 *          has been parsed
 *********************************/
size_t URI::read_host_port(const string& uri_string, size_t offset)
{
    size_t part_end = uri_string.find_first_of("/#?", offset);
    if (part_end == string::npos)
        part_end = uri_string.length();

    size_t port_offset = uri_string.find(":", offset);
    if ((port_offset == string::npos) || (port_offset > part_end))
        port_offset = part_end;

    host = uri_string.substr(offset, port_offset - offset);
    if (port_offset < part_end)
    {
        port = atoi(uri_string.substr(port_offset + 1,
            part_end - port_offset - 1).c_str());
    }

    return part_end;
}

/*********************************
 * Name:    read_host_port
 * Purpose: read the path from the URI string, if specified
 * Receive: uri_string: the URI string to be parsed
 *          offset:     the offset, the position to start parsing
 * Return:  the offset indicates that the part before this offset
 *          has been parsed
 *********************************/
size_t URI::read_path_details(const string& uri_string, size_t offset)
{
    size_t unparsed_end = uri_string.length();

    // Once you hit the beginning of the fragment, that's the end of the
    // URI.  Since it's nice to know where our limits are, let's check for
    // that first.
    size_t fragment_offset = uri_string.find("#", offset);
    if (fragment_offset != string::npos)
    {
        fragment = uri_string.substr(fragment_offset + 1);
        unparsed_end = fragment_offset;
    }

    size_t query_offset = uri_string.find("?", offset);
    if ((query_offset != string::npos) && (query_offset < unparsed_end))
    {
        query = uri_string.substr(query_offset + 1,
            unparsed_end - query_offset - 1);
        unparsed_end = query_offset;
    }

    path = uri_string.substr(offset, unparsed_end - offset);

    return uri_string.length();
}


void URI::set_protocol(const string& protocol)
{
    this->protocol = protocol;
}

void URI::set_host(const string& host)
{
    this->host = host;
}

void URI::set_path(const string& path)
{
    this->path = path;
}

void URI::Clear_port()
{
    port = UNDEFINED_PORT;
}

void URI::set_port(unsigned port)
{
    this->port = port;
}

void URI::set_query(const string& query)
{
    this->query = query;
}

void URI::set_fragment(const string& fragment)
{
    this->fragment = fragment;
}
