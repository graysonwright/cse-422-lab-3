#include "Proxy_Worker.h"
#include <sstream>
using namespace std;


/*
 * Purpose: constructor
 * receive: cs is the socket that is already connected to the requesting 
 *          client
 * Return:  none
 */
Proxy_Worker::Proxy_Worker(TCP_Socket *master_sock, string resize, string quality, HTTP_Request* header, queue<TCP_Socket*> slaveSocks)
{
    client_sock = master_sock;
    this->resize = resize;
    this->quality = quality;
    slaves = slaveSocks;

    masterRequest = header;

    port = 80; // For a full blown proxy, the server information should be 
               // obtained from each request. However, we simply assume it 
               // to be 80 for our labs.

    server_uri = URI::parse(header->get_host());
               // Must be obtain from each request.
}

/*
 * Purpose: destructor
 * receive: none
 * Return:  none
 */
Proxy_Worker::~Proxy_Worker() {
    if(server_uri != NULL)
    {
        delete server_uri;
    }

    if(client_request != NULL)
    {
        delete client_request;
    }

    if(server_response != NULL)
    {
        delete server_response;
    }
    if(client_request != NULL)
    {
        //delete client_request;
    }
    if(server_response != NULL)
    {
        //delete server_response;
    }
    server_sock.Close();

}

/*
 * Purpose: handles a request by requesting it from the server_uri 
 * Receive: none
 * Return:  none
*/
void Proxy_Worker::handle_request() {
    string buffer;

    //get HTTP request from the client
    cout << "Getting request from client..." << endl;
    if(!get_request_from_client()) 
    {
        cerr << "No request from client!" << endl;
        return;
    }  

    cout << "Received request:" << endl;
    cout << "==========================================================" 
         << endl;
    cout << client_request;
    cout << "==========================================================" 
         << endl << endl;

    cout << "Checking request..." << endl;
    //make sure we're pointing to a server
    if(server_uri == NULL) 
    {
        return_response_to_client(403);
        cout << "No server! Reject this request by a forbidden" << endl;
        return;
    }
    else
    {
        if(server_uri->get_host().find("facebook") != string::npos)
        {
            cout << "Request to URI contains facebook." << endl;
            return_response_to_client(403);
            return;
        }
    }
    cout << "Done" << endl;

    cout << "Forwarding request to server..." << endl;
    if(!forward_request_to_server()) {
        return;
    }
  
    //return the response to the client
    return_response_to_client();
  
    return;
}

/*
 * Purpose: receives the request from a client and parse it.
 * Receive: None
 * Return:  a boolean indicating if getting the request was succesful or 
 *          not
 */
bool Proxy_Worker::get_request_from_client() {
    //get the request from the client
    try
    {
        client_request = masterRequest;
        if(client_request == NULL)
        {
            cerr << "Unable to parse this request."<< endl;
            return false;
        }
        string uri_string = "http://";
        string server_host, server_path;
        client_request->get_host(server_host);
        server_path = client_request->get_uri();
        uri_string += server_host;
        uri_string += server_path;
        server_uri = URI::parse(uri_string);
        // CSE422 F12 TA: I suggest you to leave this line on.
        // The request from our simple client does not come with this
        // field: If-Modified-Since. However, for real browsers, this
        // field is always set. If the requesting object is not modified
        // the server will not reply anything.
        client_request->set_header_field("If-Modified-Since", "0");
        // We will handle the situation that server_uri is invalid (NULL)
        // later.
    }
    // Return an internal server_urierror code (this should never happen, 
    // hopefully)
    catch(string msg)
    {
        cout << msg << endl;
        HTTP_Response res(500);
        res.send_no_error(*client_sock);
        // BROADCAST THE ERROR RESPONSE TO THE SLAVES AS WELL.
        return false;
    }
    //catch an error that should be returned to the client as a response
    catch (unsigned code) {
        HTTP_Response res(code);
        res.send_no_error(*client_sock);
        // BROADCAST THE ERROR RESPONSE TO THE SLAVES AS WELL.
        return false;
    }

    return true;
}

/*
Purpose: Forwards a client request to the server
Receive: None
Return: a boolean indicating if forwarding the request was succesful or not
*/
bool Proxy_Worker::forward_request_to_server() {
    //pass the client request to the server
    try
    {
        server_sock.Connect(*server_uri);
    }
    catch(string msg)
    {
        cout << "Unable to connect to server." << endl;
        HTTP_Response res(404);
        res.send_no_error(*client_sock);
        // BROADCAST THE ERROR RESPONSE TO THE SLAVES AS WELL.
        server_sock.Close();
        return false; 
    }

    try
    {
        client_request->send(server_sock);

        string response_header, response_data;
        server_sock.read_header(response_header, response_data);
        server_response = HTTP_Response::parse(response_header.c_str(), 
                                               response_header.length());

        if(server_response == NULL)
        {
            HTTP_Response res(404);
            res.send_no_error(*client_sock);
            // BROADCAST THE ERROR RESPONSE TO THE SLAVES AS WELL.
            server_sock.Close();
            return false; 
        }

        int bytes_left = server_response->get_content_len();
        int bytes_received = 0;
        server_response->content.clear();
        int i = 0;


        if(server_response->is_chunked() == false)
        {    
            do
            {
                server_response->content += response_data;
                bytes_left -= response_data.length();
                bytes_received += response_data.length();

                response_data.clear();
                server_response->receive_data(server_sock, 
                                              response_data, bytes_left);
                
                //cout << "bytes received: " << bytes_received << endl;
                //cout << "bytes left:     " << bytes_left << endl;
            } while(bytes_left > 0);
        }
        else
        {
            int chunk_len = get_chunk_size(response_data);
            cout << "Chunked encoding transfer" << endl;
            cout << "chunk length: " << chunk_len << endl;

            while(1)
            {
                //cout << "chunk_len: " << chunk_len << endl;
                //cout << "response_data len: " << response_data.length() << endl;
                if(response_data.length() < chunk_len)
                {
                    server_response->receive_data(server_sock, 
                                                  response_data,
                                    chunk_len - response_data.length());
                }
                else
                {
                    stringstream out;
                    out << std::hex << chunk_len;
                    // convert chunk_len (as int) back to string

                    server_response->content.append(out.str());// put it back
                    server_response->content.append("\r\n"); // add the newline
                    server_response->content.append(
                            response_data.substr(0, chunk_len + 2)
                    );
                    // The +2 here is to include the CLRF between chunks.
                    // response_data 0 - chunk_len is the data of the chunk
                    // There is a CRLF between chunks.
                    server_response->content.append("\r\n"); // add the newline
                    
                    //server_response->receive_data(server_sock,
                    //                              response_data,
                    //                chunk_len - response_data.length());
                    // receive more data.
                    server_response->receive_line(server_sock,
                                                  response_data);
                    server_response->receive_line(server_sock,
                                                  response_data);

                    response_data = 
                        response_data.substr(chunk_len + 2,
                                response_data.length() - chunk_len - 2);
                    // remove the already processed chunk data and the CLRF

                    chunk_len = get_chunk_size(response_data);
                    // get the size of next chunk

                    cout << "chunk length: " << chunk_len << endl;

                    if(chunk_len == 0)
                    {
                        server_response->content.append("0\r\n");
                        break;
                    }
                }
            }
            //cout << server_response->content.length() << endl;
        }
    }
    // return an internal server_urierror code (this should never happen,
    // hopefully)
    catch(string msg)
    {
        cout << msg << endl;
        HTTP_Response res(500);
        res.send_no_error(*client_sock);
        // BROADCAST THE ERROR RESPONSE TO THE SLAVES AS WELL.
        server_sock.Close();
        return false;
    }
    server_sock.Close();

    return true;
}

/*
 * Purpose: Return a response to a client
 * Receive: None
 * Return: a boolean indicating if returning the request was succesful or not (always true for now)
*/
bool Proxy_Worker::return_response_to_client() {
    string buffer;
    server_response->set_header_field("Server", "MSU/CSE/FS12");

    cout << "Returning response to client ..." << endl; 
    cout << "==========================================================" 
         << endl;
    cout << server_response;
    cout << "==========================================================" 
         << endl;
    server_response->send_no_error(*client_sock);

    // CHECK THE CONTENT TYPE (USE get_header_value to get the value of field Content-Type)
    string value;
    server_response->get_header_value("Content-Type", value);
    if(value.find("image") != string::npos) 
    {
        // The response is an image
        // DO THE IMAGE COMPRESSION HERE
        URI *uri = URI::parse(masterRequest->get_uri());
        server_response->store_content(uri);
        server_response->convert_content(uri, resize, quality);
        server_response->read_content(uri);
    
        // BROADCAST THE RESPONSE TO ALL SLAVES
        broadcast_response_to_slaves(server_response);
    }
    
    return true;
}

/*
Purpose: Return a response to a client with no content
Receive: None
Return: a boolean indicating if returning the request was succesful or not (always true for now)
*/
bool Proxy_Worker::return_response_to_client(int status_code) {
    HTTP_Response proxy_res(status_code);
    stringstream ss;
    int content_length = int(proxy_res.content.length());
    ss << content_length;

    proxy_res.set_header_field("Content-Length", ss.str());
    proxy_res.send_no_error(*client_sock);
    // BROADCAST THE RESPONSE TO ALL SLAVES

    return true;
}


int Proxy_Worker::get_chunk_size(string &data)
{
    int chunk_len;          // The value we want to obtain
    int chunk_len_str_end;  // The var to hold the end of chunk length string 
    std::stringstream ss;   // For hex to in conversion

    chunk_len_str_end = data.find("\r\n"); // Find the first CLRF
    string chunk_len_str = data.substr(0, chunk_len_str_end);
    // take the chunk length string out

    // convert the chunk length string hex to int
    ss << std::hex << chunk_len_str;
    ss >> chunk_len;

    // reorganize the data
    // remove the chunk length string and the CLRF
    data = data.substr(chunk_len_str_end + 2, 
                       data.length() - chunk_len_str_end - 2);   

    //cout << "chunk_len_str: " << chunk_len_str << endl;
    //cout << "chunk_len:     " << chunk_len << endl;  
    return chunk_len;
}



void Proxy_Worker::broadcast_response_to_slaves(HTTP_Response *res)
{
    //BROADCAST RESPONSE TO ALL SLAVES
    while(!slaves.empty()) {
      cout << "Sending response to a slave" << endl;
      TCP_Socket* sock = slaves.front();
      slaves.pop();
      res->send_no_error(*sock);
      sock->Close();
      delete sock;
    }
}

