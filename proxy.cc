#include "proxy.h"

using namespace std;

/*********************************
 * Name:    ClientHandler
 * Purpose: Handles a single web request
 * Receive: The data socket pointer
 * Return:  None
 *********************************/
void connection_handler(TCP_Socket *master_sock, string resize, string quality, HTTP_Request *request, queue<TCP_Socket*> slaveSocks)
{
    Proxy_Worker* pw = new Proxy_Worker(master_sock, resize, quality, request, slaveSocks);
    pw -> handle_request();
    delete pw;
}

/*
 * Purpose: Contains the main server loop that handles requests by 
 *          spawing child processes
 * Receive: argc is the number of command line params, argv are the 
 *          parameters
 * Return:  0 on clean exit, and 1 on error
*/
int main(int argc, char *argv[]) {
    TCP_Socket listen_sock;
    TCP_Socket *client_sock;

    std::queue<TCP_Socket*> slave_queue; // A queue to hold the information of
                                         // currently registered slave clients.
                                         // You can replace this with any data 
                                         // structure you prefer.

    string resize("100%"); // default value is 100%
    string quality("100%"); // default value is 100%

    unsigned short int port = 0;

    pid_t pid;

    // Parses the arguments
    parse_args(argc, argv, resize, quality);

    cout << "resizing images to "<< resize 
         << " with " << quality << " quality."<< endl;

    //bind to a port and listen
    try
    {
        listen_sock.Bind(port);
        listen_sock.Listen();
    }
    catch (string msg)
    {
        cout << msg << endl;
        exit(1);
    }

    listen_sock.get_port(port);
    cout << "Proxy running at " << port << "..." << endl;

    //start the infinite server loop
    while(1) {
        //accept incoming connections
        try
        {
            if((client_sock = listen_sock.Accept()) == NULL)
            {
                cout << "Cannot accept ...";
                continue;
            }
        }
        catch (string msg)
        {
            cout << msg << endl;
            continue;
        }

        cout << endl << endl << endl;

        /****************************************
         * receive the first header line to check
         * if this request is from a slave
         ***************************************/
        // RECEIVE THE FIRST HEADER LINE
        HTTP_Request *request = HTTP_Request::receive(*client_sock);
        string host = request->get_host();

        // CHECK IF THIS REQUEST IS FROM A SLAVE OR A MASTER
        // BY SEARCHING FOR "SLAVE" IN THE FIRST HEADER LINE
        size_t slave_start = host.find("SLAVE");
        if(slave_start != string::npos) {
            // Keep track of slave socket
            slave_queue.push(client_sock);

            // output some information
            cout << "New slave enqueued." << endl;
            continue; // TO ACCEPT MORE SLAVE
        }
        else // THIS REQUEST IS FROM MASTER
        {
            cout << "Master client connected." << endl;
        }

        // For incoming requests that reach here, it must be master client
        // create new process
        pid = fork();

        // if pid < 0, the creation of process is failed.
        if(pid < 0)
        {
            cerr << "Unable to fork new child process." << endl;
            exit(1);
        }
        // if pid == 0, this is the child process
        else if(pid == 0)
        {
            // Child process invoke a ConnectionHandler function
            // to handle this connection
            // Because the proxy has already received the first 
            // header line, we need to pass it to the handler.
            // We also pass the resize and quality requirement
            // to the handler.
            connection_handler(client_sock, resize, quality, request, slave_queue);

            // CLOSE THE client_sock
            break; // Exit this main loop and terminate this child process
        }
        else
        // if pid > 0, this is the parent process
        // Parent process continues the loop to accept new incoming 
        // connections.
        {
            //WHEN REACHING HERE, YOU NEED TO RESET THE slave_queue, BECAUSE
            //THE SLAVES ARE BEING HANDLED BY A MASTER.
            while(!slave_queue.empty()) {
              TCP_Socket *sock = slave_queue.front();
              slave_queue.pop();
              sock->Close();
              delete sock;
            }
        }

        // The child process has done handleing this connection
        // Terminate the child process
    }

    if(pid == 0)
    {
        cout << "Child process terminated." << endl;
    }
    // The parent process
    else
    {
        // close the listening sock
        if(listen_sock.Close() < 0)
        {
            cerr << "Error when closing listening socket." << endl;
            exit(1);
        }
        cout << "Parent process termianted." << endl;
    }

    return 0;
}

