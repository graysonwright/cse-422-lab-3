#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include "HTTP_Request.h"
#include "HTTP_Response.h"
#include "URI.h"
#include "TCP_Socket.h"
#include "Proxy_Worker.h"

#include <queue>

using namespace std;

int port = 80;
hostent *server = NULL;


void help_message(char *argv[])
{
    cout << "Usage " << argv[0] << " [options]" << endl;
    cout << "The following options are available:" << endl;
    cout << "    -resize dimension" << endl;
    cout << "       Specifies the target dimensions of the resized images." 
         << endl;     
    cout << "       The dimension has to be widthxheight, e.g., 100x200."
         << endl;
    cout << "       Width and height are separated by the character x."
         << endl;
    cout << "    -quality value" << endl;
    cout << "       Specifies the desired quality the images being "
         << endl;
    cout << "       converted to." << endl;
    cout << "       The value must be in percentage, e.g., 80%." << endl;
}


/*
 * Purpose: check if the content in str is a number
 * Receive: the string to be check
 * Return:  true if str is holding a number, false otherwise
 */
bool is_number(const string& str)
{
    for(int i = 0; i < str.length(); i++)
    {
       if(isdigit(str[i]) == false)
       {
           return false;
       }
    }
    return true;
}

/*
 * Purpose: check if the resize string is in the required format widthxheight
 * Receive: the string to be check
 * Return:  true if the format is correct, false otherwise
 */
bool check_resize(const string resize)
{
    // check if there is an x in between
    int x_pos = resize.find("x");
    int width, height;

    // The string does not even have an x in it.
    if(x_pos == string::npos)
    {
        return false;
    }
    
    //check if the dimensions are integers
    if(is_number(resize.substr(0, x_pos)) == false){return false;}
    if(is_number(resize.substr(x_pos + 1)) == false){return false;}

    return true;
}

/*
 * Purpose: check if the quality string is in the required format value%
 * Receive: the string to be check
 * Return:  true if the format is correct, false otherwise
 */
bool check_quality(const string quality)
{
    // check if it is ending in %
    int x_pos = quality.find("%");

    if(x_pos != (quality.length() - 1))
    {
        return false;
    }
    
    //check if the quality is integer
    if(is_number(quality.substr(0, x_pos)) == false){return false;}

    return true;
}

/*********************************
 * Name:    parse_argv
 * Purpose: parse the parameters
 * Recieve: argv and argc
 * Return:  none
 *********************************/
void parse_args(int argc, char *argv[], string& resize, string& quality)
{
    char *endptr; // for strtol

    for(int i = 1; i < argc; i++)
    {
        if((!strncmp(argv[i], "-h", 2)) ||
           (!strncmp(argv[i], "-H", 2)))
        {
            help_message(argv);
            exit(1);
        }
        else if(!strncmp(argv[i], "-resize", 7))
        {
            resize = string(argv[++i]);
            if(check_resize(resize) == false)
            {
                cerr << "Invalid resize parameter, ex: 320x480." << endl;
                exit(1);
            }
        }
        else if(!strncmp(argv[i], "-quality", 8))
        {
            quality = string(argv[++i]);
            if(check_quality(quality) == false)
            {
                cerr << "Invalid quality parameter, ex: 80%." << endl;
                exit(1);
            }
        }
        else{
            cerr << "Invalid parameter:" << argv[i] << endl;    
            help_message(argv);
            exit(1);
        }
    }
}

