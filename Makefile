# CSE 422 Lab 2 makefile

# Options to set when compiling/linking the project.
CXXFLAGS=-g
LDFLAGS=

# The name of the executable to generate.
TARGET=client proxy


# The objects that should be compiled from the project source files (expected
# to correspond to actual source files, e.g. URI.o will come from URI.cc).
#
# You will want to add the name of your driver object to this list.
client_OBJS=HTTP_Message.o \
	HTTP_Request.o \
	HTTP_Response.o \
	TCP_Socket.o\
	URI.o \
	client.o 

proxy_OBJS=HTTP_Message.o \
	HTTP_Request.o \
	HTTP_Response.o \
	TCP_Socket.o\
	URI.o \
	Proxy_Worker.o \
	proxy.o


# Have everything built automatically based on the above settings.
all: $(TARGET)

.cc.o:
	g++ -o $@ $(CXXFLAGS) -c $< -DBUFFER_SIZE=4096

client: $(client_OBJS)
	g++ -o $@ $^ $(LDFLAGS)

proxy: $(proxy_OBJS)
	g++ -o $@ $^ $(LDFLAGS)

clean:
	$(RM) $(TARGET) $(proxy_OBJS) $(client_OBJS)


# Dependencies follow (i.e. which source files and headers a given object is
# built from).
TCP_Socket.o: TCP_Socket.h TCP_Socket.cc
URI.o: URI.cc URI.h
HTTP_Message.o: HTTP_Message.cc HTTP_Message.h
HTTP_Request.o: HTTP_Request.cc HTTP_Request.h HTTP_Message.h URI.o TCP_Socket.o
HTTP_Response.o: HTTP_Response.cc HTTP_Response.h HTTP_Message.h URI.o TCP_Socket.o
client.o: client.cc client.h HTTP_Request.o HTTP_Response.o
Proxy_Worker.o: Proxy_Worker.cc Proxy_Worker.h HTTP_Request.o HTTP_Response.o URI.o
proxy.o: proxy.cc proxy.h Proxy_Worker.o
