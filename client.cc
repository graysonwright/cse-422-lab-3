// Example driver/solution for Lab 2.

#include <climits>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <netdb.h>
#include <string>
#include <spawn.h>
#include <signal.h>
#include "client.h"
#include "HTTP_Request.h"
#include "HTTP_Response.h"
#include "TCP_Socket.h"

using namespace std;

string client(char* argv[], char* server_addr, 
            char* proxy_addr, char *slave_filename);

int main(int argc, char* argv[])
{
    char* server_addr = NULL;
    char* proxy_addr  = NULL;
    char* slave_filename = NULL;
    bool display = false;
    bool is_slave = false;

    parse_args(argc, argv, &server_addr, &proxy_addr, &slave_filename, display);

    if(server_addr == NULL)
    {
        cout << "You did not specify the host address." << endl;
        help_message(argv[0], cout);
        exit(1);
    }

    if(!strncmp(server_addr, "SLAVE", 5))
    {
        is_slave = true;
    }

    srand ( time(NULL) );

    pid_t last_pid = -1;

    // Loop (only once if master, forever if slave
    while(1)
    {
        // Get response from server
        string received_file = client(argv, server_addr, proxy_addr, slave_filename);

        if(display == true)
        {
            if(last_pid != -1)
            {
                kill(last_pid, SIGKILL);
                system("killall eog");
            }

            int sleep_us = (rand()%10) * 500000;
            usleep(sleep_us);

            pid_t pid = fork();
            if(pid == 0 && display == true)
            {
                char cmd[] = "eog";
                char* param[] = {cmd, (char *)received_file.c_str(), NULL};
                execvp(cmd, param);
            }
            else
            {
                last_pid = pid;
            }
        }

        if(is_slave == false)
        {
            break;
        }
    }
}

string client(char* argv[], char* server_addr, 
            char* proxy_addr, char* slave_filename)
{
    URI           *server_uri  = NULL;
    URI           *proxy_uri   = NULL;
    HTTP_Request  *request     = NULL;
    HTTP_Response *response    = NULL;
    FILE          *out         = NULL;
    bool          is_slave     = false;

    /***PARSING THE ADDRS RECEIVED TO URI OBJECTS***/
    // Must have a server to get data from
    if(server_addr == NULL)
    {
        cout << "You did not specify the host address." << endl;
        help_message(argv[0], cout);
        exit(1);
    }

    server_uri = URI::parse(server_addr);
    if(server_uri == NULL) // If URI parsing is failed
    {
        cout << "Unable to parse host address: " << server_addr << endl;
        cout << endl;
        help_message(argv[0], cout);
        exit(1);
    }

    // Proxy is an otional argument
    // If a proxy URI is specified, the client connects to the proxy and
    // downlods from the proxy. Otherwise, the client connects to the host 
    // and downloads from the host.
    proxy_uri = NULL; 
    if(proxy_addr != NULL)
    {
        proxy_uri = URI::parse(proxy_addr);
        if(proxy_uri == NULL)
        {
            cout << "Unable to parse proxy address: " << proxy_addr << endl;
            cout << endl;
            help_message(argv[0], cout);
            exit(1);
        }
        if(proxy_uri->is_port_defined() == false)
        {
            cout << "Proxy port is not defined, assumed to be 8080" << endl;
            proxy_uri->set_port(8080);
        }
    }

    // TCP_Socket class to handle TCP communications.
    TCP_Socket client_sock;
    if(proxy_addr == NULL)
    // If proxy is not specified, connect to the host directly.
    {
        try{
            // Connect to the target server.
            client_sock.Connect(*server_uri);
        }
        // Give up if sock is not created correctly.
        catch(string msg)
        {
            cout << msg << endl;
            cout << "Unable to connect to server: " 
                 << server_uri->get_host() << endl;
            delete server_uri;
            exit(1);
        }
    }
    else // proxy is specified, connect to proxy
    {
        try
        {
            // Connect to the proxy, instead of server
            client_sock.Connect(*proxy_uri);
        }
        // Give up if sock is not created correctly.
        catch(string msg)
        {
            cout << msg << endl;
            cout << "Unable to connect to proxy: " 
                 << proxy_uri->get_host() << endl;
            delete server_uri;
            delete proxy_uri;
            exit(1);
        }
    }
    /***END OF PARSING THE ADDRS RECEIVED TO URI OBJECTS***/








    // Check if the path contains the keywork slave or SLAVE
    if(!strncmp(server_addr, "SLAVE", 5))
    {
        // If this client is slave client, we need to modify its
        // path, so that we can create the "special" request that
        // identifies itself as slave client.
        string slave_path("SLAVE/");
        if(slave_filename == NULL)
        {
            slave_path.append("slave");
        }
        else
        {
            slave_path.append(slave_filename);
        }
        server_uri->set_path(slave_path.c_str()); 
        is_slave = true;
    }


    /***SENDING THE REQUEST TO THE SERVER***/
    // Send a GET request for the specified file.
    // No matter connecting to the server or the proxy, the request is 
    // alwasy destined to the server.
    request = HTTP_Request::create_GET_request(server_uri->get_path());
    request->set_host(server_uri->get_host());
    request->set_header_field("Connection", "close");

    try 
    {   
        request->send(client_sock);
    }   
    catch(string msg)
    {   
        cerr << msg << endl;
    }   

    // output the request
    cout << endl << endl << "Request sent..." << endl;
    cout << "==========================================================" 
         << endl;
    cout << request;
    cout << "==========================================================" 
         << endl;

    delete request; // We do not need it anymore
    /***END OF SENDING REQUEST***/







    /***RECEIVING RESPONSE HEADER FROM THE SERVER***/
    if(is_slave == true)
    {
        cout << "Waiting for master client's response from the proxy ... " 
        << endl;
    }
    // The server response is a stream starts with a header and then 
    // the data. A substring \r\n\r\n separates them
    // 
    // Read enough of the server's response to get all of the headers,
    // then have that response interpreted so we at least know what
    // happened.
    //
    // We create two strings to hold the incoming data. As described in the 
    // hanout, a HTTP message is composed of two portions, a header and a body.
    string response_header, response_data;

    // Read enough of the server's response to get all of the headers,
    // then have that response interpreted so we at least know what
    // happened.
    // The client receives the response stream. Check if the data it has
    // contains the whole header. 
    // read_header separates the header and data by finding \r\n\r\n
    response->receive_header(client_sock, response_header, response_data);

    // The HTTP_Response::parse construct a response object. and check if 
    // the response is constructed correctly. Also it tries to determine 
    // if the response is chunked or not. This program does not handle 
    // chunked encoded transfer.
    response = HTTP_Response::parse(response_header.c_str(), 
                                    response_header.length());

    // The response is illegal.
    if(response == NULL)
    {
        cerr << "Unable to parse the response header." << endl;
        // clean up if there's something wrong
        delete response;
        if(proxy_uri != NULL){
     
            delete proxy_uri;
        }
        delete server_uri;
        exit(1);
    }

    // output the response header
    cout << endl << "Response header received" << endl;
    cout << "=========================================================="
         << endl;
    cout << response;
    cout << "==========================================================" 
         << endl;

    /***END OF RECEIVING RESPONSE HEADER FROM THE SERVER***/












    /***GET REST OF THE MESSAGE BODY AND STORE IT***/
    // Open a local copy in which to store the file.
    //
    // Construct a filename with extension from the response 
    // and server_uri.
    // Because in the response, there is no filename information
    // For master clients, it has the server_uri, it can create
    // the filename. But for slave clients, we have to use 
    // the slave_filename given at the invocation.
    // 
    // We rearrange these information and place it in server_uri.
    // 
    string tmp_path = server_uri->get_path();
    string filename_ext;
    int fn_pos = tmp_path.rfind("/");
    if((fn_pos != string::npos) && 
       (fn_pos + 1 < tmp_path.length()))
    {
        filename_ext = tmp_path.substr(fn_pos + 1);
    }
    else
    {
        filename_ext = string("index.html");
    }
    cout << filename_ext << endl;

    if(is_slave == true)
    {
        string tmp_type;

        response->get_header_value("Content-Type", tmp_type);
        tmp_type = tmp_type.substr(tmp_type.find("/") + 1, 
                           tmp_type.length() - tmp_type.find("/") - 1);
        tmp_path.append(".");
        tmp_path.append(tmp_type);
        server_uri->set_path(tmp_path.c_str());

        filename_ext = tmp_path.substr(tmp_path.find("/") + 1, 
                           tmp_path.length() - tmp_path.find("/") - 1);
    }
    
    out = Open_local_copy(server_uri);
    // check
    if(!out)
    {
        cout << "Error opening local copy for writing." << endl;
        // clean up if failed
        if(proxy_addr != NULL)
        {
            delete proxy_uri;
        }
        delete server_uri;
        exit(1);
    }

    cout << endl << "Downloading rest of the file ... " << endl;

    int bytes_written = 0, bytes_left;
    int total_data;

    if(response->is_chunked() == false)
    {
        // none-chunked encoding transfer does not split the data into 
        // chunks. The header specifies a content_length. The client knows 
        // exactly how many data it is expecting. The client keeps receiving
        // the response until it gets the amount.

        cout << "Default encoding transfer" << endl;
        cout << "Content-length: " << response->get_content_len() << endl;
        bytes_left = response->get_content_len();
        do
        {
            // If we got a piece of the file in our buffer for the headers,
            // have that piece written out to the file, so we don't lose it.
            fwrite(response_data.c_str(), 1, response_data.length(), out);
            bytes_written += response_data.length();
            bytes_left -= response_data.length();
            //cout << "bytes written:" <<  bytes_written << endl;
            //cout << "data gotten:" <<  response_data.length() << endl;

            response_data.clear();
            try
            {
                // Keeps receiving until it gets the amount it expects.
                response->receive_data(client_sock, response_data, 
                                       bytes_left);
            }
            catch(string msg)
            {
                // something bad happend
                cout << msg << endl;
                // clean up
                delete response;
                delete server_uri;
                if(proxy_addr != NULL)
                {
                    delete proxy_uri;
                }
                fclose(out);
                client_sock.Close();
                exit(1);        
            }
        } while (bytes_left > 0);
    }
    else // chunked encoding
    {
        // The client program does not handle it for now.
        cout << "Chunked encoding transfer" << endl;

        // As mentioned above, receive_header function already split the
        // data from the header from us. The beginning of this respnse_data
        // now holds the first chunk size.
        //cout << response_data.substr(0,15) << endl;
        int chunk_len = get_chunk_size(response_data);
        cout << "chunk length: " << chunk_len << endl;
        total_data = chunk_len;
        while(1)
        {
            // If current data holding is less than the chunk_len, this 
            // piece of data contains part of this chunk. Receive more
            // until we have a complete chunk to store!
            if(response_data.length() <= chunk_len)
            {
                try
                {
                    // receive more until we have the whole chunk.
                    response->receive_data(client_sock, response_data, 
                                (chunk_len - response_data.length()));
                    response->receive_line(client_sock, response_data);
                    // get the blank line between chunks
                    response->receive_line(client_sock, response_data);
                    // get the next chunk size
                }
                catch(string msg)
                {
                    // something bad happend
                    cout << msg << endl;
                    // clean up
                    delete response;
                    delete server_uri;
                    if(proxy_addr != NULL)
                    {
                        delete proxy_uri;
                    }
                    fclose(out);
                    client_sock.Close();
                    exit(1);        
                }
            }
            // If current data holding is longer than the chunk size, this
            // piece of data contains more than one chunk. Store the chunk.
            else//response_data.length() >= chunk_len
            {
                fwrite(response_data.c_str(), 1, chunk_len, out);
                bytes_written += chunk_len;

                // reorganize the data, remove the chunk from it
                // the + 2 here is to consume the extra CLRF
                response_data = response_data.substr(chunk_len + 2, 
                                response_data.length() - chunk_len - 2);
                //get next chunk size
                chunk_len = get_chunk_size(response_data);
                total_data += chunk_len;
                cout << "chunk length: " << chunk_len << endl;
                if(chunk_len == 0)
                {
                    break;
                }
            }
        }
    }
    

    cout << "Download complete (" << bytes_written;
    cout << " bytes written)" << endl;
   
    // This checks if the chunked encoding transfer mode is downloading
    // the contents correctly.
    if((total_data != bytes_written) && response->is_chunked() == true)
    {
        cout << "WARNING" << endl 
             << "Data received does not match chunk size." << endl;
    }

    // If the response is not OK, something is wrong.
    // However, we still downloaded the content, because even the response
    // is not 200. The server still replies with an error page.
    if(response->get_status_code() != 200)
    {
        cerr << response->get_status_code() << " " 
             << response->get_status_desc() << endl;
    }

    // everything's done.
    client_sock.Close();

    delete response;
    delete server_uri;
    if(proxy_addr != NULL)
    {
        delete proxy_uri;
    }
    fclose(out);

    return filename_ext;
}
